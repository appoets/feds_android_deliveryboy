package com.feds.deliveryboy.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.feds.deliveryboy.api.ApiClient;
import com.feds.deliveryboy.api.ApiInterface;
import com.feds.deliveryboy.helper.GlobalData;
import com.feds.deliveryboy.R;
import com.twilio.voice.Call;
import com.twilio.voice.CallException;
import com.twilio.voice.ConnectOptions;
import com.twilio.voice.Voice;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Callback;
import retrofit2.Response;


public class CallActivity extends AppCompatActivity implements Call.Listener {

    @BindView(R.id.txtCallerName)
    TextView txtCallerName;
    @BindView(R.id.chronometer)
    Chronometer chronometer;
    @BindView(R.id.btnCall)
    FloatingActionButton btnCall;
    @BindView(R.id.imgSpeaker)
    ImageView imgSpeaker;
    @BindView(R.id.txtSpeaker)
    TextView txtSpeaker;
    @BindView(R.id.lnrSpeaker)
    LinearLayout lnrSpeaker;
    @BindView(R.id.imgMute)
    ImageView imgMute;
    @BindView(R.id.txtMute)
    TextView txtMute;
    @BindView(R.id.lnrMute)
    LinearLayout lnrMute;
    @BindView(R.id.imgHangup)
    ImageView imgHangup;

    private String strToken = "", strPhone = "";
    private String countryCode;
    private AudioManager audioManager;
    private int savedAudioMode = AudioManager.MODE_INVALID;
    private String TAG = "Twillo CallActivity";
    private Call activeCall;
    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        ButterKnife.bind(this);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setSpeakerphoneOn(false);
        setAudioFocus(false);

        apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

        strPhone = getIntent().getStringExtra("phone");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.MODIFY_AUDIO_SETTINGS}, 2);
        }

        Toast.makeText(getApplicationContext(),strPhone,Toast.LENGTH_SHORT).show();

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    retrieveAccessToken();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //Request for get twillo token
    @SuppressLint("CheckResult")
    private void retrieveAccessToken() {
        retrofit2.Call<AccessToken> call = GlobalData.api.voipToken();

        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(retrofit2.Call<AccessToken> call, Response<AccessToken> response) {
                    strToken = response.body().getAccessToken();
                    initiateCall(strToken);
                    //Log.e(TAG, "onResponse: "+ response );
            }

            @Override
            public void onFailure(retrofit2.Call<AccessToken> call, Throwable t) {

            }
        });
    }

    //Calling function
    public void initiateCall(String mToken) {
        HashMap<String, String> params = new HashMap<>();
        params.put("phone",strPhone);
        ConnectOptions connectOptions = new ConnectOptions.Builder(mToken)
                .params(params)
                .build();
        activeCall = Voice.connect(CallActivity.this, connectOptions, this);
        setCallUI();
    }

    private void setCallUI() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }

    //Click listners
    @OnClick({R.id.imgSpeaker, R.id.imgMute, R.id.imgHangup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSpeaker:
                if (audioManager.isSpeakerphoneOn()) {
                    audioManager.setSpeakerphoneOn(false);
                    imgSpeaker.setImageResource(R.drawable.ic_speaker_on);
                    setAudioFocus(false);
                } else {
                    audioManager.setSpeakerphoneOn(true);
                    imgSpeaker.setImageResource(R.drawable.ic_speaker_off);
                    setAudioFocus(true);
                }
                break;
            case R.id.imgMute:
                if (activeCall != null) {
                    if (activeCall.isMuted()) {
                        activeCall.mute(false);
                        imgMute.setImageResource(R.drawable.ic_mic_white_off_24dp);
                    } else {
                        activeCall.mute(true);
                        imgMute.setImageResource(R.drawable.ic_mic_white_off_24dp);
                    }
                } else {
                    Toast.makeText(CallActivity.this, getResources().getString(R.string.connection_not_established), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.imgHangup:
                resetUI();
                if (activeCall != null) {
                    activeCall.disconnect();
                    activeCall = null;
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        showCallHangupAlert();
    }


    //Cancel call alertdialog
    private void showCallHangupAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.call_cancel));

        builder.setPositiveButton(R.string.yes, (dialog, which) -> {
            if (activeCall != null) {
                activeCall.disconnect();
                activeCall = null;
                resetUI();
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss());
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void setAudioFocus(boolean setFocus) {
        if (audioManager != null) {
            if (setFocus) {
                savedAudioMode = audioManager.getMode();
                // Request audio focus before making any device switch.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            .build();
                    AudioFocusRequest focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(i -> {
                            })
                            .build();
                    audioManager.requestAudioFocus(focusRequest);
                } else {
                    int focusRequestResult = audioManager.requestAudioFocus(focusChange -> {
                            }, AudioManager.STREAM_VOICE_CALL,
                            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                }

                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            } else {
                audioManager.setMode(savedAudioMode);
                audioManager.abandonAudioFocus(null);
            }
        }
    }

    @Override
    public void onConnectFailure(@NonNull Call call, @NonNull CallException error) {
        setAudioFocus(false);
        Log.d(TAG, "Connect failure");
        String message = String.format("Call Error: %d, %s", error.getErrorCode(), error.getMessage());
        Log.e(TAG, message);

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        resetUI();

        Log.e(TAG, "onError: "+ error );
    }

    @Override
    public void onRinging(@NonNull Call call) {

    }

    @Override
    public void onConnected(@NonNull Call call) {
        setAudioFocus(true);
        Log.d(TAG, "Connected");
        activeCall = call;
    }

    @Override
    public void onReconnecting(@NonNull Call call, @NonNull CallException callException) {
        Log.d(TAG, "onReconnecting");
    }

    @Override
    public void onReconnected(@NonNull Call call) {

    }

    @Override
    public void onDisconnected(@NonNull Call call, @Nullable CallException error) {
        setAudioFocus(false);
        Log.d(TAG, "Disconnected");
        if (error != null) {
            String message = String.format("Call Error: %d, %s", error.getErrorCode(),
                    error.getMessage());
            Log.e(TAG, message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
        resetUI();
    }

    private void resetUI() {
        Toast.makeText(CallActivity.this, getResources().getString(R.string.call_disconnected), Toast.LENGTH_SHORT).show();
        chronometer.stop();
        finish();
    }

    //Get twillo token
    public class AccessToken {

        @SerializedName("accessToken")
        @Expose
        private String accessToken;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

    }

}
